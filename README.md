Hej.

Ten projekt ma na celu opisanie sposobu instalacji Arch Linux, wraz z kafelkowym środowiskiem graficznym i3wm.

Opis staram się przedstawić krok-po-kroku man nadzieję, że w sposób przystępny, wraz z komentarzami.

Dokładne informacje na temat i3wm wraz z niesamowicie dobrą dokumentacją znajdziesz na stronie autora projektu:
https://i3wm.org/

No i oczywiście strona samego Arch'a:
[https://archlinux.org](https://archlinux.org/)

Aby zacząć, przejdź do sekcji [Wiki](https://gitlab.com/r-gorajski/arch_linux_i3wm/-/wikis/home), czytaj, instaluj i baw się dobrze!!!
