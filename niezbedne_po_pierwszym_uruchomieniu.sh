#!/bin/bash

# Zmień uprawienia do tego pliku, za pomocą polecenia: 
# chmod +x niezbedne_po_pierwszym_uruchomieniu.sh
#
# Uruchom tes mikro skrypt za pomocą polecenia:
# ./niezbedne_po_pierwszym_uruchomieniu.sh
#
# Skrypt instaluje pakiety które powinieneś mieć 
# w mojej ocenie zainstalowane na początku po uruchomieniu 
# nowo zainstalowanego Arch Linux.
#
sudo su
#
pacman -Syu --noconfirm && \
pacman -S --noconfirm enchant mythes-en mythes-pl hunspell-en_US hunspell-pl aspell-en aspell-pl languagetool libmythes \
ttf-liberation ttf-bitstream-vera adobe-source-sans-pro-fonts ttf-droid ttf-dejavu ttf-ubuntu-font-family ttf-anonymous-pro \
gst-plugins-base gst-plugins-good gst-plugins-bad gst-plugins-ugly gst-libav \
pkgstats jre-openjdk
#
exit

